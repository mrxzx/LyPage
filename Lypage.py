# Project : Lypage
# Author : mrxiaozhuox
# Email : mrxzx@wwsg18.top

import os,sys
import library.command
from library.initialize import Initialize
from library.config import Config_ini,Config_json
from library.command import Command
import library.integration.handler as handler

###### 程序信息 ######

Version = "Beta"

###### 程序信息 ######

#初始化软件
settingPath = Initialize(Version)

class LyPage(object):
    def __init__(config):
        pass


#在运行程序时使用的函数
def Main():
    if len(sys.argv) > 1:
        result = Command(Version)
        if result[0] == "output":
            print(result[1])
        elif result[0] == "run":
            profile = Config_ini(settingPath + "config.ini","config","profile")
            cfg = Config_json(profile)
            cfg_dp = handler.Run_Configure_Dispose(cfg)
            handler.Create_Template(cfg_dp)
    else:
        print("Use \"" + Config_ini(settingPath + "command.ini","terminal","Help") + "\" for get help.")
        
#处理程序的加载方法
if __name__ == "__main__":
    Main()
else:
    print(__name__)