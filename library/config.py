# Config processing code
# Author : mrxiaozhuox

from configparser import ConfigParser
import json,os

def Config_ini(file,section,option):
    config = ConfigParser()
    config.read(file,encoding="utf-8")
    return config.get(section,option)

def Config_json(file,key=None):
    cfg = open(file,"r")
    data = json.loads(cfg.read())
    if not key == None:
        data = data[key]
    return data
    
    