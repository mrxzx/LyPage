# Lypage Handler processing code
# Author : mrxiaozhuox

import re
from library.initialize import SettingPath
from library.config import Config_ini

def Run_Configure_Dispose(array):
    val = {}
    unused = []

    if "name" in array:
        val['name'] = array['name']
    else:
        val['name'] = "lypage.html"

    if "file" in array:
        val['file'] = array['file']
    else:
        unused.append("file")

    if "variable" in array:
        val['variable'] = array['variable']
    else:
        val['variable'] = {}

    if "structure" in array:
        val['structure'] = array['structure']
    else:
        unused.append("structure")

    if not unused == []:
        str = ""
        for val in unused:
            str = str + val + ' , '        
        profile_name = Config_ini(SettingPath() + "config.ini","config","profile") 
        print("\"" + profile_name + "\" is missing some parameters : " + str[:-3])            
        return
    
    return val

def Create_Template(cfg_dp):
    text = ""

    for structure in cfg_dp['structure']:
        prefix = structure[0:- len(structure) + 1]

        settingPath = SettingPath()
        fmark = Config_ini(settingPath + "mark.ini",'mark','file')
        vmark = Config_ini(settingPath + "mark.ini",'mark','var')

        if prefix == fmark:
            if structure[1:] in cfg_dp['file']:
                text = text + "\n" + Parser(Get_Code(cfg_dp['file'][structure[1:]]),cfg_dp,vmark)
        elif prefix == vmark:
            if structure[1:] in cfg_dp['variable']:
                text = text + "\n" + Parser(cfg_dp['variable'][structure[1:]],cfg_dp,vmark)
        else:
            text = text + "\n" + Parser(structure,cfg_dp,vmark)

    Set_Code(cfg_dp['name'],text[1:])

def Get_Code(file):
    f = open(file,"r")
    return f.read()

def Set_Code(file,text):
    f = open(file,"w")
    return f.write(text)

def Parser(text,cfg_dp,vmark):
    varl = []
    varl = re.findall("\\" + vmark + "(.*?)\\" + vmark,text)
    returns = text
    if not varl == []:
        varv = {}
        for var in varl:
            if var in cfg_dp['variable']:
                varv[var] = cfg_dp['variable'][var]
                returns = returns.replace('$' + var + '$',cfg_dp['variable'][var]) 
        return returns
    else:
        return returns

    return returns