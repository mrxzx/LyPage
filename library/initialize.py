# Init processing code
# Author : mrxiaozhuox

import os
import getpass
import platform
from configparser import ConfigParser
from library.config import Config_ini

def SettingPath():
    system = platform.system()
    userName = getpass.getuser()

    if system == "Windows":
        settingPath = "C:\\Users\\" + str(userName) + "\\Documents\\DawnThingCB\\LyPage\\"
    elif system == "Linux":
        settingPath = "/home/" + userName + "/DawnThingCB/LyPage/"
    elif system == "Darwin":
        settingPath = "/Users/" + userName + "/Documents/DawnThingCB/LyPage/"
    else:
        return ""
    return settingPath

def Initialize(version):
    system = platform.system()
    settingPath = SettingPath()

    if settingPath == "":
        return

    create = os.path.exists(settingPath)
    if not create:
        os.makedirs(settingPath)

    if os.path.exists(settingPath + "config.ini"):
        init = Config_ini(settingPath + "config.ini","config","initialize")
        if init == "true" or init == "True":
                Config(system,version,settingPath)
    else:
        Config(system,version,settingPath)

    return settingPath
    

def Config(system,version,path):
        #写入信息
        config = ConfigParser()
        config.add_section("info")
        config.set("info","system",system)
        config.set("info","version",version)
        #写入配置
        config.add_section("config")
        config.set("config","profile","Lypage.json")
        config.set("config","initialize","true")
        #创建配置文件
        config.write(open(path + "config.ini","w"))

        #创建自定义命令配置
        command = ConfigParser()
        command.add_section("terminal")
        command.set("terminal","help","help")
        command.set("terminal","run","run")        
        command.set("terminal","version","version")
        command.set("terminal","info","info")
        command.set("terminal","update","update")
        command.set("terminal","logger","logger")

        command.write(open(path + "command.ini","w"))
        
        #创建自定义命令
        command = ConfigParser()
        command.add_section("mark")
        command.set("mark","file","@")
        command.set("mark","var","$")

        command.write(open(path + "mark.ini","w"))