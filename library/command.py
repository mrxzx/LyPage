# Command processing code
# Author : mrxiaozhuox


import os
import sys
from library.initialize import SettingPath
from library.config import Config_ini

Help = """
Usage:
    Lypage [option]
Options:
    $h        Display this help message
    $r        Start running the program
    $v        Display this application version
    $i        Display this program information
    $u        Update this program to the latest version
    $l        Display this program log

Config file at: $setting
"""

def Command(version):
    settingPath = SettingPath()

    command = {}

    command["help"] = Config_ini(settingPath + "command.ini","terminal","Help")
    command["run"] = Config_ini(settingPath + "command.ini","terminal","Run")
    command["version"] = Config_ini(settingPath + "command.ini","terminal","Version")
    command["info"] = Config_ini(settingPath + "command.ini","terminal","Info")
    command["update"] = Config_ini(settingPath + "command.ini","terminal","Update")
    command["logger"] = Config_ini(settingPath + "command.ini","terminal","Logger")

    if sys.argv[1] in command.values():
        cmd = sys.argv[1]
        cmd_key = list(command.keys())[list(command.values()).index(cmd)]

        if cmd_key == "help":
            Menu = Help
            Menu = Menu.replace("$h",command["help"])
            Menu = Menu.replace("$r",command["run"])
            Menu = Menu.replace("$v",command["version"])
            Menu = Menu.replace("$i",command["info"])
            Menu = Menu.replace("$u",command["update"])
            Menu = Menu.replace("$l",command["logger"])

            Menu = Menu.replace("$setting",settingPath)
            return ["output",Menu]
        elif cmd_key == "run":
            return ["run"]
        elif cmd_key == "version":
            return ["output","program Version : " + version]
        elif cmd_key == "info":
            return ["output","To edit..."]
        elif cmd_key == "update":
            return ["update"]
        elif cmd_key == "looger":
            return ["logger"]
    else:
        return ["output","Command \"" + sys.argv[1] + "\" does not exist."]