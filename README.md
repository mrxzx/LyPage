# LyPage

[![Travis](https://img.shields.io/badge/Language-Python-blue.svg)](https://www.python.org)
[![Travis](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://mit-license.org)
[![Travis](https://img.shields.io/badge/Version-Beta-red.svg)](http://www.wwsg18.com)

## 关于LyPage：

    LyPage是我用来对HTML代码进行整合的。有时候每个页面都有同一段代码，修改起来很不方便，我将要复用的代码单独分开，最后再使用LyPage打包（现在功能挺少）


## 使用方法：

1. 使用help指令查询到配置文件地址
2. 自行修改配置文件
3. 使用Lypage.json(文件名可修改)配置程序
4. 运行run指令，自动整合文档

## 配置须知：

### command.ini

主要配置程序的指令（修改后调用程序的指令也会修改）

极简的配置，不需要做解释

### config.ini

主要做一些基础配置

info:
    
    info不需要修改，是程序记录一些系统数据的

config:

    1. profile : 记录程序读取的配置文件名（默认：Lypage.json）
    2. initialize : 是否每次运行程序都重置所有配置（建议关闭）

mark:

    1. file : 读取文件的前缀符（默认@）
    2. var : 读取变量的前缀符（默认$）


## JSON格式：

### 整体Demo:

    {
        "name" : "demo/lypage.html",
        "file" : {
            "header" : "demo/header.html",
            "footter" : "demo/footter.html",
            "index" : "demo/index.html"
        },
        "variable" : {
            "username" : "mrxiaozhuox",
            "password" : "1234567890",
            "phone" : "15756382446"
        },
        "structure" : [
            "@header",
            "@index",
            "@footter"
        ]
    }

### 参数介绍:

    1. name : 整合后的文件名字
    2. file : 文件列表（Key设置一个名字，Value设置文件地址）
    3. variable : 设置变量（可在文件中直接使用）
    4. structure : 构造结构（根据需要的顺序将文件和变量排列好）

### 支持内容:

    1. 结构配置 : 支持使用文件，变量，普通字符串
       1. 文件 : "@FileName" (文件前缀@)
       2. 变量 : "$VarName" (变量前缀$)
       3. 字符串 : "Text" (字符串直接写内容，可以为HTML)
   
    2. 文件内 : 变量，普通字符串
       1. 变量 : "$VarName$" (变量名写在两个$之间)
       2. 字符串 : "Text" (字符串直接写内容，可以为HTML)

## 联系作者：

作者:mr小卓X

Q Q:3507952990

GitHub: https://github.com/DawnThingCB/LyPage

>如发现问题请 发布Issue 或 联系作者